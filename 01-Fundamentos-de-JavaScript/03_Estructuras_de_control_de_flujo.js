// Estructura "if/else":
let edad = 20;

if (edad >= 18) {
  console.log("Eres mayor de edad");
} else {
  console.log("Eres menor de edad");
}


//Estructura "switch":
let dia = 3;

switch (dia) {
  case 1:
    console.log("Lunes");
    break;

  case 2:
    console.log("Martes");
    break;

  case 3:
    console.log("Miércoles");
    break;

  default:
    console.log("No es un día válido");
    break;
}


//Estructura "for":
for (let i = 0; i < 5; i++) {
    console.log(i);
  }

  
//Estructura "while":
let i = 0;

while (i < 5) {
  console.log(i);
  i++;
}
