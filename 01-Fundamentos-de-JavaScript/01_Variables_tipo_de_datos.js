```
En JavaScript, existen varios tipos de datos que pueden ser almacenados en variables. 
A continuación, se presentan algunos ejemplos de variables y tipos de datos:
```

// Números:
let a = 10;
let b = 3.14;

// Cadenas de texto:
let nombre = "Jimdev";
let mensaje = "Hola, ¿cómo estás?";

// Arreglos
let numeros = [1, 2, 3, 4, 5];
let colores = ["rojo", "verde", "azul"];

// Objetos:
let persona = {
    nombre: "Jimdev",
    edad: 26,
    correo: "Jimdev@example.com"
  };

// Booleanos
let esMayor = true;
let esHombre = false;

// Null y undefined:
let x = null;
let y;

