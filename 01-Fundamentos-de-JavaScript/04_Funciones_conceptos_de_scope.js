// Funciones sin argumentos:
function saludar() {
    console.log('¡Hola, mundo!');
}
saludar();


// Funciones con argumentos:
function sumar(a, b) {
    return a + b;
}
console.log(sumar(2, 3));


//Funciones flecha:
const multiplicar = (a, b) => {
    return a * b;
};
console.log(multiplicar(4, 5));




// Conceptos de scope:

//Variables globales:
const nombre = 'Juan';

function saludar() {
    console.log(`¡Hola, ${nombre}!`);
}

saludar(); // Output: ¡Hola, Juan!
```
En este ejemplo, la variable nombre se declara fuera de la función, 
por lo que es una variable global y puede ser accedida desde cualquier parte del código, 
incluyendo dentro de la función saludar().
```


//Variables locales:
function sumar(a, b) {
    const resultado = a + b;
    console.log(resultado);
}

sumar(2, 3); // Output: 5
console.log(resultado); // Output: Uncaught ReferenceError: resultado is not defined
```
En este ejemplo, la variable resultado se declara dentro de la función sumar(), 
por lo que es una variable local y sólo puede ser accedida desde dentro de la función.
```

// Closure:
function contador() {
    let count = 0;

    function incrementar() {
        count++;
        console.log(count);
    }

    return incrementar;
}

const contador1 = contador();
contador1(); // Output: 1
contador1(); // Output: 2

const contador2 = contador();
contador2(); // Output: 1
```
En este ejemplo, la función contador() devuelve otra función llamada incrementar(). 
La variable count es una variable local dentro de contador(), 
pero incrementar() "recuerda" el valor de count incluso después de que contador() ha terminado de ejecutarse. 
Cada vez que se llama a contador(), se crea un nuevo closure con su propia variable count.
```